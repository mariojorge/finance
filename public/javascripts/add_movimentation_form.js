  $(document).ready(function(){
	  $("#movimentation_value").val($("#movimentation_value").val().replace(".", ","));
	  $("#movimentation_value").numeric(",");
	  $("#movimentation_value").floatnumber(",",2);

	  $("#div_quantity").hide();

	  $("#movimentation_is_fixed").click(function () {  
		if( $("#movimentation_is_fixed").is(':checked') ){
		  $("#div_quantity").show("slow");
		  $('#movimentation_is_subdivided').attr('checked', false);
		}

		if(!$("#movimentation_is_fixed").is(':checked') && !$("#movimentation_is_subdivided").is(':checked')){
		  $("#div_quantity").hide("slow");
		  $("#movimentation_quantity").val('0');
		}
	  });

	  $("#movimentation_is_subdivided").click(function () {  

		if( $("#movimentation_is_subdivided").is(':checked') ){
		  $("#div_quantity").show("slow");
		  $('#movimentation_is_fixed').attr('checked', false);
		}

		if(!$("#movimentation_is_fixed").is(':checked') && !$("#movimentation_is_subdivided").is(':checked')){
		  $("#div_quantity").hide("slow");
		  $("#movimentation_quantity").val('0');
		}
	  });
  });