class AddLocalToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :local_to_pay, :string
  end

  def self.down
    remove_column :movimentations, :local_to_pay
  end
end
