class AddCategoryIdToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :category_id, :integer
  end

  def self.down
    remove_column :movimentations, :category_id
  end
end
