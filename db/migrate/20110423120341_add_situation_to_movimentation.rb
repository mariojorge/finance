class AddSituationToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :situation, :boolean
  end

  def self.down
    remove_column :movimentations, :situation
  end
end
