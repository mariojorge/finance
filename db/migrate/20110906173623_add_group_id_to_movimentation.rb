class AddGroupIdToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :group_id, :string
  end

  def self.down
    remove_column :movimentations, :group_id
  end
end
