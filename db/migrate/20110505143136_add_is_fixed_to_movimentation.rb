class AddIsFixedToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :is_fixed, :boolean
  end

  def self.down
    remove_column :movimentations, :is_fixed
  end
end
