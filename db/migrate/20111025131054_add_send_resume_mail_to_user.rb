class AddSendResumeMailToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :send_resume_mail, :boolean
  end

  def self.down
    remove_column :users, :send_resume_mail
  end
end
