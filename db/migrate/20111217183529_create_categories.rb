class CreateCategories < ActiveRecord::Migration
  def self.up
    #drop_table :categories
    create_table :categories do |t|
      t.string :title
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    #drop_table :categories
    create_table :categories do |t|
      t.string :title

      t.timestamps
    end
  end
end
