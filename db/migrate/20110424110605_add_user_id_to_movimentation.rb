class AddUserIdToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :user_id, :integer
  end

  def self.down
    remove_column :movimentations, :user_id
  end
end
