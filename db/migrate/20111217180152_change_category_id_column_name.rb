class ChangeCategoryIdColumnName < ActiveRecord::Migration
  def self.up
    rename_column :movimentations, :category_id, :type_of_movimentation
  end

  def self.down
    rename_column :movimentations, :type_of_movimentation, :category_id
  end
end
