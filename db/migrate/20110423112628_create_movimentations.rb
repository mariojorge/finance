class CreateMovimentations < ActiveRecord::Migration
  def self.up
    create_table :movimentations do |t|
      t.integer :category_id
      t.string :description
      t.decimal :value, :precision => 14, :scale => 2
      t.date :payday

      t.timestamps
    end
  end

  def self.down
    drop_table :movimentations
  end
end

