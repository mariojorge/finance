class AddNumberToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :number, :string
  end

  def self.down
    remove_column :movimentations, :number
  end
end
