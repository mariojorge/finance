class CreateCreditcardPayments < ActiveRecord::Migration
  def self.up
    create_table :creditcard_payments do |t|
      t.date :payed_in
      t.decimal :value, :precision => 14, :scale => 2
      t.integer :month
      t.integer :year
      t.integer :creditcard_id

      t.timestamps
    end
  end

  def self.down
    drop_table :creditcard_payments
  end
end
