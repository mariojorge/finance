class AddReceiptToMovimentation < ActiveRecord::Migration
  def self.up
    add_column :movimentations, :receipt_file_name, :string
    add_column :movimentations, :receipt_content_type, :string
    add_column :movimentations, :receipt_file_size, :integer
    add_column :movimentations, :receipt_updated_at, :datetime
  end

  def self.down
    remove_column :movimentations, :receipt_updated_at
    remove_column :movimentations, :receipt_file_size
    remove_column :movimentations, :receipt_content_type
    remove_column :movimentations, :receipt_file_name
  end
end
