class CreateCreditcards < ActiveRecord::Migration
  def self.up
    create_table :creditcards do |t|
      t.string :name
      t.string :flag
      t.integer :closing_day
      t.integer :payday
      t.decimal :limit, :precision => 14, :scale => 2
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :creditcards
  end
end
