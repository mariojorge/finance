class CreateDesires < ActiveRecord::Migration
  def self.up
    create_table :desires do |t|
      t.string :title
      t.decimal :price, :precision => 14, :scale => 2
      t.string :priority
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :desires
  end
end
