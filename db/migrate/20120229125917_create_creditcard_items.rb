class CreateCreditcardItems < ActiveRecord::Migration
  def self.up
    create_table :creditcard_items do |t|
      t.date :purchased_in
      t.string :title
      t.string :number
      t.decimal :value, :precision => 14, :scale => 2
      t.integer :month_to_pay
      t.integer :year_to_pay
      t.boolean :is_fixed
      t.string :group_id
      t.integer :creditcard_id

      t.timestamps
    end
  end

  def self.down
    drop_table :creditcard_items
  end
end
