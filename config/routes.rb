Br22Devise::Application.routes.draw do

  #match "android/login" => "json#login"
  #match "android/creditcards" => "json#creditcards"
  #match "android/creditcard_items" => "json#creditcard_items"
  #match "android/categories" => "json#categories"
  #match "android/movimentations" => "json#movimentations"
  #match "android/desires" => "json#desires"

  resources :creditcard_payments

  resources :creditcard_items

  resources :creditcards

  match "report1" => "reports#report_1", :as => :report_1
  match "report2" => "reports#report_2", :as => :report_2

  resources :categories

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  get "goto" => "movimentations#goto", :as => :goto

  resources :desires

  match 'overdue' => 'movimentations#overdue', :as => :movimentations_overdue
  match 'all' => 'movimentations#all', :as => :movimentations_all
  resources :movimentations do
    member do
      get 'change_situation'
      get 'change'
      get 'delete'
    end
  end

  match 'token/create' => 'token_authentications#create', :as => :create_token
  match 'token/destroy' => 'token_authentications#destroy', :as => :destroy_token
  devise_for :users, :controllers => { :sessions => "sessions" }

  devise_scope :user do
    get "sign_in", :to => "sessions/create"
    get "sign_out", :to => "sessions/destroy"
  end

  get "home/index"
  get "home/adjust"
  get "home/send_resume_mail", :as => :send_resume_mail

  namespace :android do
    match "login" => "session#login"
    resources :categories
    resources :creditcard_items
    resources :creditcards
    resources :desires
    resources :movimentations
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => "home#welcome"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end

