class CreditcardsController < InheritedResources::Base
before_filter :authenticate_user!
  
  # GET /creditcards
  # GET /creditcards.xml
  def index
    @search = Creditcard.where("user_id = ?" , current_user.id).search(params[:search])
    @creditcards = @search.all   # load all matching records
    @creditcards = @search.relation
    @creditcards = @search.paginate :page => params[:page], :per_page => 10

    @creditcard = Creditcard.new
    @creditcard.user_id = current_user.id
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @creditcards }
      format.json { render "index.json" }
    end
  end

  # GET /creditcards/1
  # GET /creditcards/1.xml
  def show
    month = Time.now.month
    year = Time.now.year
    month = params[:month].to_i if params[:month]
    year = params[:year].to_i if params[:year]

    session[:cc_back] = request.request_uri

    @creditcard = Creditcard.find(params[:id])
    @creditcard_items = CreditcardItem.where("creditcard_id = ? AND month_to_pay = ? AND year_to_pay = ?", params[:id], month, year).order("purchased_in ASC, title ASC").all

    @total = 0
    @creditcard_items.each do |item|
      @total = @total + item.value
    end

    @next = month.to_i + 1
    if @next == 13
        @next = 1
    end

    @previous = month.to_i - 1
    if @previous == 0
        @previous = 12
    end

    @month = month
    @year = year

    @creditcard_item = CreditcardItem.new
    @creditcard_item.creditcard_id = @creditcard.id

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @creditcard }
    end
  end

  # GET /creditcards/new
  # GET /creditcards/new.xml
  def new
    @creditcard = Creditcard.new
    @creditcard.user_id = current_user.id

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @creditcard }
    end
  end

  # GET /creditcards/1/edit
  def edit
    @creditcard = Creditcard.find(params[:id])
  end

  # POST /creditcards
  # POST /creditcards.xml
  def create
    @creditcard = Creditcard.new(params[:creditcard])

    respond_to do |format|
      if @creditcard.save
        format.html { redirect_to(creditcards_path, :notice => 'Novo Cartao de Credito cadastrado.') }
        format.xml  { render :xml => @creditcard, :status => :created, :location => @creditcard }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @Creditcard.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /creditcards/1
  # PUT /creditcards/1.xml
  def update
    @creditcard = Creditcard.find(params[:id])

    respond_to do |format|
      if @creditcard.update_attributes(params[:creditcard])
        format.html { redirect_to(creditcards_path, :notice => 'Cartao de Credito Atualizado.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @creditcard.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /creditcards/1
  # DELETE /creditcards/1.xml
  def destroy
    @creditcard = Creditcard.find(params[:id])
    @creditcard.destroy

    respond_to do |format|
      format.html { redirect_to(creditcards_url, :notice => 'Cartao de Credito removido.') }
      format.xml  { head :ok }
    end
  end
end
