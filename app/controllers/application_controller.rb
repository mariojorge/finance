class ApplicationController < ActionController::Base
  protect_from_forgery

  include Mobylette::RespondToMobileRequests

  #before_filter :set_language
  
  #def set_language
  #  I18n.locale = params[:locale] if params[:locale]
  #end
  
  #def default_url_options(options={})
  #  #logger.debug "default_url_options is passed options: #{options.inspect}\n"
  #  { :locale => I18n.locale }
  #end

     layout :layout_by_resource

  protected

  def layout_by_resource
    if devise_controller?
      if action_name == 'edit'
        'application'
      elsif action_name == 'create'
        'devise'
      elsif action_name == 'new'
        'devise'
      else
        "blank"
      end
    else
      #if is_mobile_request?
        #"mobile"
      #else
        "application"
      #end
    end
  end
end
