class MovimentationsController < ApplicationController
  before_filter :authenticate_user!
  #layout "admin"

  # GET /movimentations
  # GET /movimentations.xml
  def index
    month = Time.now.month
    year = Time.now.year
    month = params[:month].to_i if params[:month]
    year = params[:year].to_i if params[:year]
  
    initial_date = Date.new(year, month, 1)
    final_date = (initial_date + 1.month) - 1.day
  
    # Busca todas as movimentações a receber
    @search = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday BETWEEN ? AND ?", current_user.id, 1, initial_date, final_date).order("payday ASC").search(params[:search])
    @movimentations_in = @search.all   # load all matching records
    @movimentations_in = @search.relation
    
    # Busca todas as movimentações a pagar
    @search2 = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday BETWEEN ? AND ?", current_user.id, 2, initial_date, final_date).order("payday ASC").search(params[:search])
    @movimentations_out = @search2.all   # load all matching records
    @movimentations_out = @search2.relation

    # Busca faturas dos cartões de créditos cadastrados
    @creditcard_movimentations = Array.new
    Creditcard.where("user_id = ?", current_user.id).all.each do |creditcard|
      @creditcard_movimentations.push(creditcard.to_m(month, year)) if creditcard.total_to_pay(month, year) > 0
    end
    
    @next_month = (initial_date + 1.month).month.to_i
    @previous_month = (initial_date - 1.month).month.to_i
    
    @next_year = (initial_date + 1.year).year.to_i
    @previous_year = (initial_date - 1.year).year.to_i

    @month = month
    @year = year
    
    session[:back] = request.request_uri
    
    @movimentation = Movimentation.new({:user_id => current_user.id, :quantity => 1, :type_of_movimentation => 2})
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @movimentations }
    end
  end

  # GET /movimentations/1
  # GET /movimentations/1.xml
  def show
    @movimentation = Movimentation.find(params[:id])
    
    @movimentations = Movimentation.where("group_id = ? AND user_id = ?", @movimentation.group_id, current_user.id).all
    
    session[:back] = request.request_uri
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @movimentation }
    end
  end

  # GET /movimentations/new
  # GET /movimentations/new.xml
  def new
    @movimentation = Movimentation.new
    @movimentation.user_id = current_user.id
    @movimentation.quantity = 1

    respond_to do |format|
      format.html 
      format.xml  { render :xml => @movimentation }
    end
  end

  # GET /movimentations/1/edit
  def edit
    @movimentation = Movimentation.find(params[:id])
    #quantity = Movimentation.where("description = ? AND value = ?", @movimentation.description, @movimentation.value).all.size
    #quantity > 1 ? @movimentation.quantity = quantity : @movimentation.quantity = 0
  end

  # POST /movimentations
  # POST /movimentations.xml
  def create
    params[:movimentation][:value] = params[:movimentation][:value].gsub(',','.')
    params[:movimentation][:situation] = false
    params[:movimentation][:situation] = 1 if params[:movimentation][:quantity] == 0
    @movimentation = Movimentation.new(params[:movimentation])
    @movimentation.group_id = UUIDTools::UUID.random_create.to_s 

    respond_to do |format|
      if @movimentation.save
        format.html { redirect_to(session[:back] ? session[:back] : movimentations_url, :notice => 'Nova Movimentacao cadastrada.') }
        format.xml  { render :xml => @movimentation, :status => :created, :location => @movimentation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @movimentation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /movimentations/1
  # PUT /movimentations/1.xml
  def update
    @movimentation = Movimentation.find(params[:id])
    params[:movimentation][:value] = params[:movimentation][:value].gsub(',','.')

    # Atualizar valor em todas as movimentacoes, exceto quando esta sendo quitada uma movimentacao
    unless params[:movimentation][:situation]
      movimentations = Movimentation.where("group_id = ? AND user_id = ?", @movimentation.group_id, current_user.id)

      # Atualiza valor
      if params[:movimentation][:save_value_only_in_this] == "false"
        movimentations.each do |movimentation|
          movimentation.update_attributes(:value => params[:movimentation][:value]) if movimentation.situation == false
        end
      end

      # Atualiza outros campos
      movimentations.each do |movimentation|
        movimentation.update_attributes(:type_of_movimentation => params[:movimentation][:type_of_movimentation], :description => params[:movimentation][:description], :local_to_pay => params[:movimentation][:local_to_pay] , :category_id => params[:movimentation][:category_id])
      end
    end

    respond_to do |format|
      if @movimentation.update_attributes(params[:movimentation])
        format.html { redirect_to(session[:back] ? session[:back] : movimentations_path, :notice => 'Movimentacao atualizada.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @movimentation.errors, :status => :unprocessable_entity }
      end
    end
  end

  def delete
    @movimentation = Movimentation.find(params[:id])

    respond_to do |format|
      format.html 
      format.xml  { render :xml => @movimentation }
    end
  end

  # DELETE /movimentations/1
  # DELETE /movimentations/1.xml
  def destroy
    @movimentation = Movimentation.find(params[:id])
    #@movimentation.destroy

    if params[:movimentation][:delete_all] == "true"
      movimentations = Movimentation.where("group_id = ? AND user_id = ?", @movimentation.group_id, current_user.id)
      movimentations.each do |movimentation|
        movimentation.destroy
      end
    else
      @movimentation.destroy
    end
    #Movimentation.where("group_id ? AND user_id = ?", @movimentation.group_id, current_user.id).destroy_all

    respond_to do |format|
      format.html { redirect_to(session[:back] ? session[:back] : movimentations_url, :notice => 'Movimentacao removida com sucesso.') }
      format.xml  { head :ok }
    end
  end

  def change_situation
    @movimentation = Movimentation.find(params[:id])
    @movimentation.situation ? @movimentation.situation = false : @movimentation.situation = true

    respond_to do |format|
      if @movimentation.save()
        format.html { redirect_to(session[:back] ? session[:back] : movimentations_path, :notice => 'Movimentation was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { redirect_to(movimentations_path, :notice => '#Fail') }
        format.xml  { render :xml => @movimentation.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def change
    @movimentation = Movimentation.find(params[:id])
    
    if params[:back]
      session[:back] = @movimentation
    end
  end
  
  def overdue
    @search = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday < ? AND situation = 0", current_user.id, 1, Date.today).order("situation ASC, payday ASC").search(params[:search])
    @movimentations_in = @search.all   # load all matching records
    @movimentations_in = @search.relation

    @search2 = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday < ? AND situation = 0", current_user.id, 2, Date.today).order("situation ASC,payday ASC").search(params[:search])
    @movimentations_out = @search2.all   # load all matching records
    @movimentations_out = @search2.relation

    session[:back] = request.request_uri

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @movimentations }
    end
  end
  
  def goto
    #month = params
    respond_to do |format|
      format.html {redirect_to :action => "index", :month => params[:date][:month], :year => params[:date][:year]}
    end
  end

  def all
    @search = Movimentation.where("user_id = ? AND payday < NOW()", current_user.id).order("payday DESC").search(params[:search])
    @movimentations = @search.all   # load all matching records
    @movimentations = @search.relation
    @movimentations = @search.paginate :page => params[:page], :per_page => 10

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @movimentations }
    end
  end

end

