class CreditcardItemsController < ApplicationController
  before_filter :authenticate_user!

  # GET /creditcard_items/new
  # GET /creditcard_items/new.xml
  def new
    @creditcard_item = CreditcardItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @creditcard_item }
    end
  end

  # GET /creditcard_items/1/edit
  def edit
    @creditcard_item = CreditcardItem.find(params[:id])
  end

  def create
    params[:creditcard_item][:value] = params[:creditcard_item][:value].gsub(',','.')
    @creditcard_item = CreditcardItem.new(params[:creditcard_item])

    @creditcard = Creditcard.find(@creditcard_item.creditcard_id)
    respond_to do |format|
      if @creditcard_item.save
        format.html { redirect_to(session[:cc_back] ? session[:cc_back] : @creditcard, :notice => 'Nova Movimentacao no Cartao de Credito cadastrada.') }
        format.xml  { render :xml => @creditcard_item, :status => :created, :location => @creditcard }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @creditcard_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /creditcards/1
  # PUT /creditcards/1.xml
  def update
    params[:creditcard_item][:value] = params[:creditcard_item][:value].gsub(',','.')
    @creditcard_item = CreditcardItem.find(params[:id])

    @creditcard_items = CreditcardItem.where("group_id = ?", @creditcard_item.group_id).all

    @creditcard_items.each do |item|
      item.update_attributes(:title => params[:creditcard_item][:title], :value => params[:creditcard_item][:value])
    end

    respond_to do |format|
      if @creditcard_item.update_attributes(params[:creditcard_item])
        format.html { redirect_to(session[:cc_back] ? session[:cc_back] : @creditcard_item.creditcard, :notice => 'Movimentacao do Cartao de Credito Atualizada.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @creditcard.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /creditcards/1
  # DELETE /creditcards/1.xml
  def destroy
    @creditcard_item = CreditcardItem.find(params[:id])
    @creditcard_item.destroy_creditcard_items

    respond_to do |format|
      format.html { redirect_to(session[:cc_back] ? session[:cc_back] : @creditcard_item.creditcard, :notice => 'Movimentacao do Cartao de Credito removida.') }
      format.xml  { head :ok }
    end
  end
end
