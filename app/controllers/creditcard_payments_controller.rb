class CreditcardPaymentsController < ApplicationController

  def new
    @creditcard_payment = CreditcardPayment.new(:creditcard_id => params[:creditcard_id], :month => params[:month], :year => params[:year], :value => params[:value].gsub(".", ","))

    @creditcard = Creditcard.find(params[:creditcard_id])

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @creditcard_item }
    end
  end

  def create
    params[:creditcard_payment][:value] = params[:creditcard_payment][:value].gsub(',','.')
    @creditcard_payment = CreditcardPayment.new(params[:creditcard_payment])

    @creditcard = Creditcard.find(@creditcard_payment.creditcard_id)
    respond_to do |format|
      if @creditcard_payment.save
        format.html { redirect_to(session[:back] ? session[:back] : @creditcard, :notice => 'Realizado pagamento de Fatura de Cartao de Credito.') }
        format.xml  { render :xml => @creditcard_item, :status => :created, :location => @creditcard }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @creditcard_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /creditcard_payment/1
  # DELETE /creditcard_payment/1.xml
  def destroy
    @creditcard_payment = CreditcardPayment.find(params[:id])
    @creditcard_payment.destroy

    respond_to do |format|
      format.html { redirect_to(session[:cc_back] ? session[:cc_back] : @creditcard_payment.creditcard, :notice => 'Movimentacao do Cartao de Credito removida.') }
      format.xml  { head :ok }
    end
  end
end
