class HomeController < ApplicationController
  before_filter :authenticate_user!, :except => [:send_resume_mail, :welcome]

  def index
  end
  
  def adjust
    @movimentations = Movimentation.all
    @movimentations.each do |movimentation|
      unique_id = UUIDTools::UUID.random_create.to_s
      if movimentation.group_id == "" or movimentation.group_id == nil
        others = Movimentation.where("description = ? AND user_id = ? AND local_to_pay = ? AND group_id = '' ", movimentation.description, movimentation.user_id, movimentation.local_to_pay).all
        others.each do |other|
          other.update_attributes(:group_id => unique_id)
        end
      end
    end
    
    respond_to do |format|
      format.html { }
      format.xml  { head :ok }
    end    
  end
  
  def send_resume_mail
    User.all.each do |user|
      if user.send_resume_mail == true
         MovimentationMailer.resume_email(user).deliver
      end
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @movimentations }
    end
  end

  def welcome
    @user = User.new

    respond_to do |format|
      if current_user == nil
        format.html { render :layout => 'home' }
        format.mobile { render :layout => 'mobile' }
      else
        format.html { redirect_to(movimentations_path, :notice => nil) }
        format.mobile { redirect_to(movimentations_path, :notice => nil) }
      end
      format.xml  { render :xml => @movimentations }
    end
  end
end

