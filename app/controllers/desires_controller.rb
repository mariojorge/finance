class DesiresController < ApplicationController
  before_filter :authenticate_user!
  
  # GET /desires
  # GET /desires.xml
  def index
    @search = Desire.where("user_id = ?", current_user.id).search(params[:search])
    @desires = @search.all   # load all matching records
    @desires = @search.relation
    @desires = @search.paginate :page => params[:page], :per_page => 10
    
    @total = 0
    @desires.each do |desire|
      @total = @total + desire.price
    end

    # Para formulario de cadastro
    @desire = Desire.new
    @desire.user_id = current_user.id

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @desires }
    end
  end

  # GET /desires/1
  # GET /desires/1.xml
  def show
    @desire = Desire.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @desire }
    end
  end

  # GET /desires/new
  # GET /desires/new.xml
  def new
    @desire = Desire.new
    @desire.user_id = current_user.id

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @desire }
    end
  end

  # GET /desires/1/edit
  def edit
    @desire = Desire.find(params[:id])
  end

  # POST /desires
  # POST /desires.xml
  def create
    params[:desire][:price] = params[:desire][:price].gsub(',','.')
    @desire = Desire.new(params[:desire])

    respond_to do |format|
      if @desire.save
        format.html { redirect_to(desires_path, :notice => 'Novo desejo cadastrado.') }
        format.xml  { render :xml => @desire, :status => :created, :location => @desire }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @desire.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /desires/1
  # PUT /desires/1.xml
  def update
    @desire = Desire.find(params[:id])
    params[:desire][:price] = params[:desire][:price].gsub(',','.')

    respond_to do |format|
      if @desire.update_attributes(params[:desire])
        format.html { redirect_to(@desire, :notice => 'Desejo atualizado.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @desire.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /desires/1
  # DELETE /desires/1.xml
  def destroy
    @desire = Desire.find(params[:id])
    @desire.destroy

    respond_to do |format|
      format.html { redirect_to(desires_url, :notice => 'Desejo removido.') }
      format.xml  { head :ok }
    end
  end
end
