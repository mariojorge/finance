class Android::MovimentationsController < ApplicationController
  before_filter :authenticate_user!

  # GET /movimentations.json
  def index
    user_id = params[:user_id]
    @search = Movimentation.where("user_id = ?", user_id).search(params[:search])
    @movimentations = @search.all

    respond_to do |format|
      format.json
    end
  end

  # GET /movimentations/1.json
  def show
    user_id = params[:user_id]
    @movimentation = Movimentation.where("user_id = ?", user_id).find(params[:id])
    
    rescue ActiveRecord::RecordNotFound
    respond_to do |format|
      if @movimentation
        format.json
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # POST /movimentations.json
  def create
    @movimentation = Movimentation.new(params[:movimentation])

    respond_to do |format|
      if @movimentation.save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # GET /movimentations/1/edit.json
  def edit
    begin
      @movimentation = Movimentation.find(params[:id])
      found = true
      save = false
        save = @movimentation.update_attributes(params[:movimentation])
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      save = false
    end

    respond_to do |format|
      if found and save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # DELETE /movimentations/1.json
  def destroy
    begin
      @movimentations = Movimentation.where("group_id = ?", params[:id])
      found = true
      destroyed = false
      begin
        if @movimentations.size > 0
          @movimentations.each do |movimentation|
            movimentation.destroy
          end
          @movimentations = Movimentation.where("group_id = ?", params[:id])
          if @movimentations.size <= 0
            destroyed = true
          end
        end
      rescue NoMethodError
        destroyed = false
      end
    rescue ActiveRecord::RecordNotFound
      found = false
      destroyed = false
    end

    respond_to do |format|
      if found and destroyed
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  def change_situation
    @movimentation = Movimentation.find(params[:id])
    @movimentation.situation ? @movimentation.situation = false : @movimentation.situation = true

    respond_to do |format|
      if @movimentation.save()
        format.html { redirect_to(session[:back] ? session[:back] : movimentations_path, :notice => 'Movimentation was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { redirect_to(movimentations_path, :notice => '#Fail') }
        format.xml  { render :xml => @movimentation.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def change
    @movimentation = Movimentation.find(params[:id])
    
    if params[:back]
      session[:back] = @movimentation
    end
  end
  
end

