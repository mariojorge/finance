# encoding: utf-8 
class Android::SessionController < ApplicationController

  def login
    #token = ""
    message = ""
    status_code = 401

    email = params[:email]
    password = params[:password]

    if request.format != :json
      status_code = 406
      message = "A requisição deve ser json"
    end

    if email.nil? or password.nil?
      status_code = 400
      message = "A requisição deve conter email e senha"
    else

      @user = User.find_by_email(email.downcase)

      if @user.nil?
        status_code = 401
        message = "E-mail ou senha inválido(s)"
      else
        #@user.ensure_authentication_token!
        if not @user.valid_password?(password)
          status_code = 401 
          message = "E-mail ou senha inválido(s)"
        else
          status_code = 200 
          token = @user.authentication_token
        end
      end

    end

    respond_to do |format|
      if token.nil?
        format.json { render :status => status_code, :json=> {:message => message} }
      else
        format.json { render :status => status_code, :json=> {:token => token, :user_id => @user.id, :name => @user.name} }
      end
    end
  end

end
