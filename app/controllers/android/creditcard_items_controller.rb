class Android::CreditcardItemsController < ApplicationController
  before_filter :authenticate_user!

  # GET /creditcard_items
  def index
    user_id = params[:user_id]
    creditcards = Creditcard.where("user_id = ?", user_id).all
    a = Array.new
    creditcards.each do |creditcard|
      a.push creditcard.id
    end
    @search = CreditcardItem.where("creditcard_id IN (?)", a).search(params[:search])
    @creditcard_items = @search.all

    respond_to do |format|
      format.json
    end
  end

   # POST /creditcard_items.json
  def create
    @creditcard_item = CreditcardItem.new(params[:creditcard_item])

    respond_to do |format|
      if @creditcard_item.save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # DELETE /creditcard_items/1.json
  def destroy
    begin
      @creditcard_items = CreditcardItem.where("group_id = ?", params[:id])
      found = true
      destroyed = false
      begin
        if @creditcard_items.size > 0
          @creditcard_items.each do |creditcard_item|
            creditcard_item.destroy
          end
          @creditcard_items = CreditcardItem.where("group_id = ?", params[:id])
          if @creditcard_items.size <= 0
            destroyed = true
          end
        end
      rescue NoMethodError
        destroyed = false
      end
    rescue ActiveRecord::RecordNotFound
      found = false
      destroyed = false
    end

    respond_to do |format|
      if found and destroyed
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

end
