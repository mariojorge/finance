class Android::CreditcardsController < InheritedResources::Base
  before_filter :authenticate_user!
  
  # GET /creditcards.json
  def index
    user_id = params[:user_id]
    @search = Creditcard.where("user_id = ?", user_id).search(params[:search])
    @creditcards = @search.all

    respond_to do |format|
      format.json
    end
  end

  # GET /creditcards/1.json
  def show
    user_id = params[:user_id]
    @creditcard = Creditcard.where("user_id = ?", user_id).find(params[:id])
    
    rescue ActiveRecord::RecordNotFound
    respond_to do |format|
      if @creditcard
        format.json
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # POST /creditcards.json
  def create
    @creditcard = Creditcard.new(params[:creditcard])

    respond_to do |format|
      if @creditcard.save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # GET /creditcards/1/edit.json
  def edit
    begin
      @creditcard = Creditcard.find(params[:id])
      found = true
      save = false
        save = @creditcard.update_attributes(params[:creditcard])
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      save = false
    end

    respond_to do |format|
      if found and save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # DELETE /creditcards/1.json
  def destroy
    begin
      @creditcard = Creditcard.find(params[:id])
      found = true
      destroyed = false
        destroyed = @creditcard.destroy
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      destroyed = true
    end

    respond_to do |format|
      if found and destroyed
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

end
