class Android::CategoriesController < InheritedResources::Base
  before_filter :authenticate_user!
  
  # GET /categories.json
  def index
    user_id = params[:user_id]
    @search = Category.where("user_id = ?", user_id).search(params[:search])
    @categories = @search.all

    respond_to do |format|
      format.json
    end
  end

  # GET /categories/1.json
  def show
    user_id = params[:user_id]
    @category = Category.where("user_id = ?", user_id).find(params[:id])
    
    rescue ActiveRecord::RecordNotFound
    respond_to do |format|
      if @category
        format.json
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # POST /categories.json
  def create
    @category = Category.new(params[:category])

    respond_to do |format|
      if @category.save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # GET /categories/1/edit.json
  def edit
    begin
      @category = Category.find(params[:id])
      found = true
      save = false
        save = @category.update_attributes(params[:category])
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      save = false
    end

    respond_to do |format|
      if found and save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # DELETE /categories/1.json
  def destroy
    begin
      @category = Category.find(params[:id])
      found = true
      destroyed = false
        destroyed = @category.destroy
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      destroyed = true
    end

    respond_to do |format|
      if found and destroyed
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

end
