class Android::DesiresController < ApplicationController
  before_filter :authenticate_user!
  
  # GET /desires.json
  def index
    user_id = params[:user_id]
    @search = Desire.where("user_id = ?", user_id).search(params[:search])
    @desires = @search.all

    respond_to do |format|
      format.json 
    end
  end

  # GET /desires/1.json
  def show
    user_id = params[:user_id]
    @desire = Desire.where("user_id = ?", user_id).find(params[:id])
    
    rescue ActiveRecord::RecordNotFound
    respond_to do |format|
      if @desire
        format.json
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # POST /desires.json
  def create
    @desire = Desire.new(params[:desire])

    respond_to do |format|
      if @desire.save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # GET /desires/1/edit.json
  def edit
    begin
      @desire = Desire.find(params[:id])
      found = true
      save = false
        save = @desire.update_attributes(params[:desire])
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      save = false
    end

    respond_to do |format|
      if found and save
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end

  # DELETE /desires/1.json
  def destroy
    begin
      @desire = Desire.find(params[:id])
      found = true
      destroyed = false
        destroyed = @desire.destroy
        rescue NoMethodError
    rescue ActiveRecord::RecordNotFound
      found = false
      destroyed = true
    end

    respond_to do |format|
      if found and destroyed
        format.json { render :status => 200, :json=> {:status => "Success"} }
      else
        format.json { render :status => 200, :json=> {:status => "Failure"} }
      end
    end
  end
end
