class ReportsController < ApplicationController
  before_filter :authenticate_user!

  def report_1
    @movimentations = Movimentation.where("user_id = ?", current_user.id).order("payday ASC").all

    @first_year = @movimentations.first.payday.year.to_i
    @last_year = @movimentations.last.payday.year.to_i

    @movimentations_in = Movimentation.where("user_id = ? AND type_of_movimentation = 1", current_user.id).order("payday ASC").all
    @movimentations_out = Movimentation.where("user_id = ? AND type_of_movimentation = 2", current_user.id).order("payday ASC").all

    @categories = Category.where("user_id = ?", current_user.id).order("title ASC").all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def report_2
    @movimentations = Movimentation.where("user_id = ?", current_user.id).order("payday ASC").all

    @first_year = @movimentations.first.payday.year.to_i
    @last_year = @movimentations.last.payday.year.to_i

    @movimentations_in = Movimentation.where("user_id = ? AND type_of_movimentation = 1", current_user.id).order("payday ASC").all
    @movimentations_out = Movimentation.where("user_id = ? AND type_of_movimentation = 2", current_user.id).order("payday ASC").all

    @creditcards = Creditcard.where("user_id = ?", current_user.id).all

    respond_to do |format|
      format.html # index.html.erb
    end
  end
end
