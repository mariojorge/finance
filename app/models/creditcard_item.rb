class CreditcardItem < ActiveRecord::Base
  belongs_to :creditcard

  attr_accessor :quantity, :is_subdivided

  validates_presence_of :title, :value

  before_create :initiate
  after_create :save_others

  def initiate
    if (self.group_id == nil)
      # Gera um group_id unico
      self.group_id = UUIDTools::UUID.random_create.to_s
      # calcula o mes de pagamento
      self.purchased_in.day.to_i >= self.creditcard.closing_day ? self.month_to_pay = self.purchased_in.month.to_i + 1 : self.month_to_pay = self.purchased_in.month.to_i
      # calcula o ano de pagamento e atualiza o mes para janeiro
      self.year_to_pay = self.purchased_in.year.to_i
      if self.month_to_pay > 12
        self.month_to_pay = 1
        self.year_to_pay = self.year_to_pay + 1
      end

      self.number = "1 de #{self.quantity}" if self.quantity.to_i > 1 and self.is_fixed == false
    end
  end

  def save_others
    if (self.quantity.to_i > 1)
      for i in 2..self.quantity.to_i do
        # calcula o ano e o mes da proxima parcela
        self.month_to_pay += 1
        if self.month_to_pay > 12
          self.month_to_pay = 1
          self.year_to_pay = self.year_to_pay + 1
        end
        self.is_fixed ? self.number = nil : self.number = "#{i} de #{self.quantity}"
        CreditcardItem.create!(:purchased_in => self.purchased_in, :title => self.title, :number => self.number, :value => self.value, :month_to_pay => self.month_to_pay, :year_to_pay => self.year_to_pay, :is_fixed => self.is_fixed, :group_id => self.group_id, :creditcard_id => self.creditcard_id)
      end
    end
  end

  def destroy_creditcard_items
    CreditcardItem.where("group_id = ?", self.group_id).all.each do |item|
      item.destroy
    end
  end
end
