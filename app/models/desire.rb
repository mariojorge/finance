class Desire < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :title, :price, :priority
end
