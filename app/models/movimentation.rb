class Movimentation < ActiveRecord::Base
  belongs_to :user
  belongs_to :category

  has_attached_file :receipt

  attr_accessor :quantity, :save_value_only_in_this, :is_subdivided, :delete_all, :credicard_id

  validates_numericality_of :quantity, :on => :create
  validates_presence_of :description, :value, :on => :create

  before_create :set_initial_number
  after_create :save_others
  
  def set_initial_number
    if self.quantity.to_i > 1
      self.number = "1 de #{self.quantity}"
    end
  end

  def save_others
    if self.quantity.to_i > 1
      actual_date = self.payday
      number = 1
      for i in 2..self.quantity.to_i do
        actual_date = actual_date + 1.month
        number = number + 1
        Movimentation.create :type_of_movimentation => self.type_of_movimentation, :description => self.description, :quantity => 0, :value => self.value, :payday => actual_date, :local_to_pay => self.local_to_pay, :situation => false, :number => "#{number} de #{self.quantity}", :user_id => self.user_id, :is_fixed => self.is_fixed, :group_id => self.group_id, :category_id => self.category_id
      end
    end
  end
  
  def status
    if self.category_id == 1
      if self.situation
        "Recebido"
      else
        "A Receber"
      end
    else
      if self.situation
        "Pago"
      else
        "A pagar"
      end
    end
  end
  
  def type
    if self.type_of_movimentation == 1
      "A Receber"
    else
      "A Pagar"
    end
  end
  
  def is_last?
    movimentation = Movimentation.order("id DESC").find_by_group_id(self.group_id)
    if movimentation.id == self.id
      true
    else
      false
    end
  end

end

