class Creditcard < ActiveRecord::Base
  belongs_to :user
  has_many :creditcard_items
  has_many :creditcard_payments

  validates_presence_of :name, :closing_day, :payday

  def total_to_pay(month, year)
    total = 0
    self.creditcard_items.each do |credicard_item|
      if credicard_item.month_to_pay == month and credicard_item.year_to_pay == year
        total += credicard_item.value
      end
    end
    total
  end

  # Converte uma fatura de cartão de crédito em movimentação
  def to_m(month, year)
      movimentation = Movimentation.new(:description => "#{self.name}", :payday => Date.new(year, month, self.payday), :is_fixed => true, :value => self.total_to_pay(month, year), :local_to_pay => "Fatura Cartao", :situation => self.is_invoice_payed?(month, year), :credicard_id => self.id)
  end

  def is_invoice_payed?(month, year)
    payment = CreditcardPayment.find_by_month_and_year_and_creditcard_id(month, year, self.id)
    payment == nil ? false : true
  end

  def get_payment(month, year)
    CreditcardPayment.find_by_month_and_year_and_creditcard_id(month, year, self.id)
  end

end
