class User < ActiveRecord::Base
  has_many :movimentations
  has_many :desires
  has_many :categories
  has_many :creditcards
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :send_resume_mail
  
  validates_inclusion_of :send_resume_mail, :in => [true, false]

  before_create :ensure_authentication_token
end

