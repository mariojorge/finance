class Category < ActiveRecord::Base
  belongs_to :user
  has_many :movimentations, :dependent => :nullify

  validates_presence_of :title, :user_id

  def sum_movimentation_by_month_and_year(type, year, month)
    sum = 0
    month_begins = Date.new(year, month, 1)
    month_ends = Date.new(year, month, 1).at_end_of_month
    self.movimentations.each do |movimentation|
      if (movimentation.payday >= month_begins && movimentation.payday <= month_ends && movimentation.type_of_movimentation == type)
        sum = sum + movimentation.value
      end
    end
    return sum
  end

  # ABREVIATURA DA FUNCAO sum_movimentation_by_month_and_year
  def sum_by_my(type, year, month)
    self.sum_movimentation_by_month_and_year(type, year, month)
  end
end
