ActiveAdmin::Dashboards.build do

  # Define your dashboard sections here. Each block will be
  # rendered on the dashboard in the context of the view. So just
  # return the content which you would like to display.
  
  # == Simple Dashboard Section
  # Here is an example of a simple dashboard section
  #
     section "Ultimos Usuarios Cadastrados" do
       ul do
         User.order("created_at DESC").limit(10).all.collect do |user|
           li link_to("#{user.name.upcase} (#{user.email}) ", admin_user_path(user))
         end
       end
     end

     section "Ultimas Visitas" do
       ul do
         User.order("current_sign_in_at DESC").limit(10).all.collect do |user|
           li link_to("#{user.name.upcase} (#{user.current_sign_in_at.to_s_br}) ", admin_user_path(user))
         end
       end
     end
  
  # == Render Partial Section
  # The block is rendered within the context of the view, so you can
  # easily render a partial rather than build content in ruby.
  #
  #   section "Recent Posts" do
  #     div do
  #       render 'recent_posts' # => this will render /app/views/admin/dashboard/_recent_posts.html.erb
  #     end
  #   end
  
  # == Section Ordering
  # The dashboard sections are ordered by a given priority from top left to
  # bottom right. The default priority is 10. By giving a section numerically lower
  # priority it will be sorted higher. For example:
  #
  #   section "Recent Posts", :priority => 10
  #   section "Recent User", :priority => 1
  #
  # Will render the "Recent Users" then the "Recent Posts" sections on the dashboard.

  ActiveAdmin.register Movimentation do
    index do
      column :id
      column :description
      column :value
      column :payday
      column :situation
      column :number
      column :user_id
      default_actions
    end
  end

  ActiveAdmin.register User do
    index do
      column :id
      column :name
      column :email
      column :sign_in_count
      column :current_sign_in_at
      column :send_resume_mail
      default_actions
    end
  end

  ActiveAdmin.register Desire do
    index do
      column :id
      column :title
      column :price
      column :priority
      column :user_id
      default_actions
    end
  end



end
