class MovimentationMailer < ActionMailer::Base
  default :from => "naoresponda@mario-jorge.com"
  
  def resume_email(user)
    @user = user
    @url = "http://financeiro.mario-jorge.net"
    @movimentations_in_end_today = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday = ? AND situation = 0", user.id, 1, Date.today).order("id ASC")
    @movimentations_out_end_today = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday = ? AND situation = 0", user.id, 2, Date.today).order("id ASC")
    @movimentations_in_overdue = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday < ? AND situation = 0", user.id, 1, Date.today).order("payday ASC")
    @movimentations_out_overdue = Movimentation.where("user_id = ? AND type_of_movimentation = ? AND payday < ? AND situation = 0", user.id, 2, Date.today).order("payday ASC")

    # Envia e-mail se existir alguma movimentacao nas buscas acima
    if (@movimentations_in_end_today.size > 0 or @movimentations_out_end_today.size > 0 or @movimentations_in_overdue.size > 0 or @movimentations_out_overdue.size > 0)
      mail(:to => user.email, :subject => "Contas do Dia #{Date.today.to_s_br}")
    end
  end
end
