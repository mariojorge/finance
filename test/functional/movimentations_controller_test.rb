require 'test_helper'

class MovimentationsControllerTest < ActionController::TestCase
  setup do
    @movimentation = movimentations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:movimentations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create movimentation" do
    assert_difference('Movimentation.count') do
      post :create, :movimentation => @movimentation.attributes
    end

    assert_redirected_to movimentation_path(assigns(:movimentation))
  end

  test "should show movimentation" do
    get :show, :id => @movimentation.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @movimentation.to_param
    assert_response :success
  end

  test "should update movimentation" do
    put :update, :id => @movimentation.to_param, :movimentation => @movimentation.attributes
    assert_redirected_to movimentation_path(assigns(:movimentation))
  end

  test "should destroy movimentation" do
    assert_difference('Movimentation.count', -1) do
      delete :destroy, :id => @movimentation.to_param
    end

    assert_redirected_to movimentations_path
  end
end
